# GoFundMe-Fundraiser-Clone-GoFundMe-Script-DOD-IT-Solution
 Doditsolutions offers customized and scalable Crowdfunding Software Development service. We offer crowdfunding solutions, with the motive to help individuals, companies, and entrepreneur Our highly skilled team develops software with the latest tools and advanced technologies. With our readymade crowdfunding software, you can save time and reduce cost as through our readymade software we deliver the fundraising script which is the clone of Kickstarter, gofundme and Indiegogo. Our GoFundMe Fundraise Crowdfunding Clone Script will help our clients available with a full-fledged, user-friendly, and state-of-the-art platform on their websites. Crowdfunding is on the increase because there are more funding needs and startups than Venture Capitalists can, or will, fund. Time and again crowdfunding has proved to be a successful platform that could raise the bar for your business. GoFundMe Fundraise Crowdfunding clone is one of the best options for raising funds for your organizations activities and for undertaking projects. Crowdfunding activities include everything legal, including exhibitions, games, interviews, meeting people, concerts, magic shows, project development, and other activities for fundraising.
Technology Used 
• PHP Language
 • Core PHP Framework 
• MySQL Database 
• Server: Any Linux Server. 
Modules Included 
• Crowd funding Clone Script 
• Panels included for the above: User, Guest User, Fundraiser Panel, Investor Panel, Super Admin and Advanced features.  
Crowd funding Portal Features 
SUPER ADMIN PANEL 
• Login
 Admin can login with entering Email Id and Password. 
• Dashboard
 Admin can view details of Counts and Statistics of users Counts etc.
 • Sub Admin 
The admin can appoint multiple sub admin to assist with the platform. Admin will set roles for sub admin.
 • Manage Projects 
Admin can view list of Projects & can Manage Projects. Admin can activate, deactivate projects and can backlist projects. 
• Withdraw Request
Admin can view list of withdraw request and can search withdraw request using title, from and to date etc. 
• Manage Content 
Admin can view and manage content for pages added in the website. 
• Payment History 
Admin can view details of payments done by Investor for their projects. 
• Manage Commission 
Admin can view commission for the Projects and can manage it. Admin can activate and deactivate the commission. 
• Manage Currency 
Admin can add currency & can manage currency. Admin can activate and deactivate currencies.
 • Categories 
Admin can add details of Categories and Sub Categories and can manage them. Admin can activate and deactivate categories and sub categories
. • RTL Compatibility 
Compatible writing with Right to Left language like Arabic or Hebrew and offers bidirectional support on all browsers. 
• Customer Management 
Create customer profiles in multiple groups like registered and guest customers, view and manage wish list, coupons, etc.
 • Languages
 The entire user side of the script can be customized to the language of your choice. You can Add, Edit and Set Translations.
 • Advertising 
Add & Update Banners to Advertisement. These ads will then be showcased to the Users browsing through your Website. 
FUNDRAISER PANEL
 • Hybrid Apps 
Users get fully branded IOS & Android Apps for your Crowd funding Clone Script Website
. • OTP Login 
Supports Social Media Login (FB and Twitter). Sign up with a mobile number to allow Login using OTP (One Time Password) for a password less login access to your Website. 
• Sign Up 
You must sign up to become a student. You need to provide your name, email and password to sign up your account. If you already have an account use Login option. 
• Dashboard Fundraiser panel is provided with feature created functionalities and at constant time it’s easy to use and manage.
 • Filter Filter Courses by Category, Prices, Level, Language and Ratings. 
• Create a Project
Fundraiser will produce a project by adding details or Project, Images, Videos, Description, Location details etc. 
• Classification 
The Fundraiser project is further classified on the basis of Category, and Sub Category.
 • Search
 Fundraiser can search for projects on the portal by entering the Keywords. 
• Filter 
Can search by Project class, Location & Sorting which is able to offer the relevant result. 
• Post Projects
 Fundraiser can post their projects on the website where he can view details of Investors. 
• Manage Projects 
Fundraiser can manage different projects which are being posted in the website with their category. 
• View Collaborators
 Fundraiser can view the profile of Collaborator and can add Collaborators of their need.
 • Commission Management 
Admin has the ability to manage the commission value vary by adding what commission share to be taken. 
• Payment Gateway 
Fundraiser uses PayPal method with wallet for ease, to ensure that projects can be created, funded, commissions etc. are transferred to the wallet. 
• Staff Management 
In Fundraiser Platform admin has the authority to add roles for each staff available.
 • Analytics Management 
Its includes sending messages and newsletter to subscribed users and manage Google Analytics. 
• View Projects 
Fundraiser can view the list of posted projects with the current status of the project. 
• Share Projects 
Fundraiser can share projects on other Social Media for those who are interested in it. 
• View Recent Activities Fundraiser can view the recent activities on the portal and keep track of the activities.
 • Fund Request Fundraiser can request for withdrawing fund and can keep track of payment details and status. 
INVESTOR PANEL
 • Dashboard 
Instructors get a separate panel for managing courses. List of Active Course, Pending Course, Free Courses, Paid Courses.
 • Registration 
Register your project with details. Enter accurate details of Segments to the vision of the Project. 
• Browse Projects 
Browse through different projects and understand deep in depth details of Projects.
 • Fund
Once you find the project that appeals to you and you have gone through the details you can choose it to start funding. 
• Favourite 
Projects Investors can Favourite the project which the found Interesting. You can view the list of the projects & can also view details of projects. 
• Contact Project 
Investor can view the Project Creators Profile & can Contact them for Project Details. 
• Post Comments 
Investor can comment on the Project which are posted on the Portal.
 • View Project Investor
 can view details of each project, Video, Pledge, Location, FAQ etc. 
ADVANCED FEATURES 
• Social Media LogIn 
This allows clients to enter your online fundraiser website through social media logins. It gives your consumers the seamless fundraiser experience. 
• Social Share 
You can share your Projects or details through Social Media to your other colleagues.
 • Live Chat 
With live chat integration reply to queries, make easy live conversation & ensure support. 
• ERP Feature
 ERP package enable a corporation to take care of master lists of all customers and vendors, the product it sells, the fabric company procures, chart of accounts list, worker knowledge & knowledge that company owns. 
• Record New Entry
 Suppose a brand new marketer should be recorded within the master knowledge. Correct marketer ID, his actual verified location, payment terms and mechanism and credit limits are recorded. Erp System take care of data entry in a reliable form
. • ERP Report
 Tools for querying info and generating unexpected reports area unit accessible within the ERP system. These tools conjointly embrace customizable dashboards, making completely different graphs and different visual representations
. • CRM 
A CRM element of ERP system principally keeps track of all of your client and sales information. This module includes options like insights of sales patterns and client behaviours, client preferences and many more.

